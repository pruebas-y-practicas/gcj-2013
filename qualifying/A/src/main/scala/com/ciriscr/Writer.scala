package com.ciriscr

import java.io.PrintWriter
/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 25/04/13
 * Time: 15:33
 */

class Writer(filepath: String) {

  private val out: PrintWriter = new PrintWriter(filepath)

  def escribir(mensaje: String){
    out.println(mensaje)
    flush
  }

  def cerrar {out.close()}
  def flush {out.flush()}
}