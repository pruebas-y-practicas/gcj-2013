package com.ciriscr

import scala.io.Source
/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 25/04/13
 * Time: 13:56
 */

object Reader {

  def read(filePath: String): Seq[Board] = {
    val file = Source.fromFile(filePath).getLines()
    val total = file.next().toInt
    for (i <- 1 to total) yield {
      new Board(file.take(5))
    }
  }

}
